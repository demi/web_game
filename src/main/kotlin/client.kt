import react.dom.render
import kotlinx.browser.document
import kotlinx.browser.window
import kotlinx.html.classes
import kotlinx.html.js.onMouseDownFunction
import org.w3c.dom.HTMLElement
import react.*
import styled.css
import styled.styledDiv

external fun clickFellThrough(vararg args: dynamic): Unit

@JsExport
data class GameInterfaceState(
    val player_x: Float,
    val player_y: Float,
    val player_z: Float,
    val fps: Float,
    val frametime: Float,
) : RState

@JsExport
var onStateChangedListeners: Array<(GameInterfaceState)->Unit> = emptyArray()

fun main() {
    window.onload = {
        // Dummy implementations of external functions.
        js("if (typeof clickFellThrough !== 'function') {window.clickFellThrough = function() { console.log('clickFellThrough called!') }};");
        render(document.getElementById("root")) {
            document.onmousedown = {event ->
                // Prevent double click here.
                if (event.detail > 1) {
                    event.preventDefault();
                }
            }
            val clicksFallThrough = "clicks_fall_through";
            document.onmouseover = { event ->
                (event.target as? HTMLElement)?.also { target ->
                    if (target.classList.contains(clicksFallThrough)) {
                        clickFellThrough(true)
                    } else {
                        clickFellThrough(false)
                    }
                    console.log("mouseover target: "+target.classList)
                }
            }
            styledDiv {
                css {
                    +WelcomeStyles.viewportContainer
                    +clicksFallThrough
                }
                attrs {
                    classes += "clicks_fall_through"
                    onMouseDownFunction = { event ->
                        event.stopPropagation();
                        clickFellThrough();
                        console.log("===> Clicked on top level div!");
                    }
                }
                child(DebugView::class) {
                }
                child(Healthbar::class) {
                    attrs {
                        hp = 100
                        mp=100
                    }
                }
                child(Welcome::class) {
                    attrs {
                        name = "Kotlin/JS"
                    }
                }
            }
        }
    }
}
