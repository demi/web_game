import kotlinx.html.js.onClickFunction
import react.RBuilder
import react.RComponent
import react.RProps
import react.RState
import styled.css
import styled.styledDiv

external interface HealthbarProps : RProps {
    var hp: Number
    var mp: Number
}

data class HealthbarState(
        val hp: Number,
        val hp_max: Number,
        val mp: Number,
        val mp_max: Number
) : RState

@JsExport
class Healthbar(props: HealthbarProps) : RComponent<HealthbarProps, HealthbarState>(props) {

    init {
        state = HealthbarState(props.hp, props.hp, props.mp, props.mp)
    }

    override fun RBuilder.render() {
        styledDiv {
            css {
                +HealthbarStyles.flexContainer
            }
            styledDiv {
                css {
                    +HealthbarStyles.healthContainer
                }
                attrs {
                    onClickFunction = { event ->
                        //console.log("Hi!");
                    }
                }
                +"HP ${state.hp}/${state.hp_max}"
            }

            styledDiv {
                css {
                    +HealthbarStyles.manaContainer
                }
                attrs {
                    onClickFunction = { event ->
                        //console.log("Hi!");
                    }
                }
                +"MP ${state.mp}/${state.mp_max}"
            }
        }
    }
}