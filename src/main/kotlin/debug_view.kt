import kotlinx.css.FontWeight
import kotlinx.css.fontWeight
import kotlinx.html.js.onClickFunction
import react.*
import styled.css
import styled.styledDiv
import styled.styledLabel

external interface DebugViewProps : RProps {
}

data class DebugViewState(
        var game: GameInterfaceState
) : RState

fun<T: Number> toFixed(v: T, n: Int): String {
    val string = "${v}";
    if (string.contains('.')) {
        val strs = string.split('.').toMutableList()
        if (n == 0) return strs.first()
        return strs.first()+"."+strs.last().take(n)
    }
    return string;
}

@JsExport
class DebugView(props: DebugViewProps) : RComponent<DebugViewProps, DebugViewState>(props) {

    init {
        state = DebugViewState(GameInterfaceState(0.0f,0.0f,0.0f, 60.0f, 1.0f/60))
        console.log(onStateChangedListeners)
        onStateChangedListeners += arrayOf({ ui_state ->
            setState {
                this.game = ui_state
            }
        });
        console.log(onStateChangedListeners)
    }

    override fun RBuilder.render() {
        styledDiv {
            css {
                +DebugViewStyles.flexContainer
            }
            styledLabel {
                css {
                    +DebugViewStyles.textContainer
                    fontWeight= FontWeight.bold
                }
                +"DEBUG"
            }
            styledLabel {
                css {
                    +DebugViewStyles.textContainer
                }
                +"x: ${toFixed(state.game.player_x, 3)} y: ${toFixed(state.game.player_y, 3)} z: ${toFixed(state.game.player_z, 3)}"
            }
            styledLabel {
                css {
                    +DebugViewStyles.textContainer
                }
                +"fps: ${toFixed(state.game.fps, 0)}"
            }
            styledLabel {
                css {
                    +DebugViewStyles.textContainer
                }
                +"frametime: ${toFixed(state.game.frametime*1000.0, 2)} ms"
            }
        }
    }
}