import kotlinx.css.*
import kotlinx.css.properties.border
import kotlinx.css.properties.boxShadow
import kotlinx.css.properties.s
import kotlinx.css.properties.transition
import styled.StyleSheet

object HealthbarStyles : StyleSheet("HealthbarStyles", isStatic = true) {
    val flexContainer by css {
        +WelcomeStyles.flexContainer
        //alignContent = Align.center
        justifyContent = JustifyContent.center
        margin(0.px)
        padding(0.px)

        //backgroundColor = Color.aliceBlue
        //color = Color.aliceBlue
        position = Position.absolute
        width = 100.pct
        left = 0.pct
        bottom = 0.pct
    }
    val healthContainer by css {
        padding(5.px)
        margin(5.px)
        borderRadius = 5.px

        backgroundColor = Color.indianRed
        color = Color.white
        fontWeight = FontWeight.bold
    }
    val manaContainer by css {
        +healthContainer
        backgroundColor = Color.indigo
    }
}
