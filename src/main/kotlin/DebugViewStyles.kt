import kotlinx.css.*
import kotlinx.css.properties.*
import styled.StyleSheet
import styled.animation
import styled.keyframes
import kotlin.math.cos
import kotlin.math.sin

object DebugViewStyles : StyleSheet("DebugViewStyles", isStatic = true) {
    val flexContainer by css {
        +WelcomeStyles.flexContainer
        flexDirection = FlexDirection.column
        alignContent = Align.flexStart
        //justifyContent = JustifyContent.center

        //backgroundColor = Color.aliceBlue
        //color = Color.aliceBlue
        position = Position.absolute
        minWidth = 20.pct
        right = 0.pct
        //height = 20.pct
        top = 0.pct

        borderRadius = 5.px

        backgroundColor = Color.black.withAlpha(0.35)
        //backgroundColor = hsl(hue, 100, 50)
        hover {
            animation(1.s, Timing.linear, iterationCount = IterationCount.infinite) {
                for(pct in IntRange(0, 100)) {
                    val t = 0.5*(1.0-cos(pct/100.0*2*kotlin.math.PI));
                    this.rule("${pct}%") {
                        transform {
                            scale(1.0+0.5*t)
                            translate(-(t*20).pct, (t*20).pct)
                        }
                        backgroundColor = Color("hsl(${(pct*3.6).toInt()}, 100%, 50%)").withAlpha(0.5);
                    }
                }
            }
        }
    }
    val textContainer by css {
        //padding(5.px)
        //margin(5.px)
        //width = 100.pct

        color = Color.white
        fontWeight = FontWeight.w500
        fontSize = 15.0.pt
    }
}
