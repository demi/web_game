import kotlinx.css.*
import kotlinx.css.properties.border
import kotlinx.css.properties.s
import kotlinx.css.properties.transition
import styled.StyleSheet

object WelcomeStyles : StyleSheet("WelcomeStyles", isStatic = true) {
    val clicksFallThrough by css {

    }
    val viewportContainer by css {
        width = 100.vw
        height = 100.vh
        position = Position.fixed
        left = 0.px
        bottom = 0.px
        margin(0.px)
    }
    val flexContainer by css {
        display = Display.flex
        flexDirection = FlexDirection.row
        flexWrap = FlexWrap.wrap
        padding(5.px)
        margin(5.px)
        alignContent = Align.center
    }
    val textContainer by css {
        padding(5.px)
        margin(5.px)
        borderRadius = 5.px

        backgroundColor = Color.deepSkyBlue
        color = Color.aliceBlue

        transition(duration = 0.15.s)
        hover {
            transition(duration = 0.15.s)
            border(3.px, BorderStyle.solid, Color.red, borderRadius)
        }
    }

    val textInput by css {
        margin(vertical = 5.px)

        fontSize = 14.px
    }
} 
